Name: Seegal Panchal
ID: 1016249
Assignment 1

-------------------------------------------------------------------

You can compile every program by typing "make" without
quotation marks in the terminal (in the folder directory).

The below only works if you compile first.

-------------------------------------------------------------------

Question 1: to run the program that outputs Carbon,
simply type ./carbon

--------------------------------------------------------------------

Question 2: to run the program that calculates Ackermanns
number, type ./ackermann [m] [n] ,where m and n are
their respective values. For example, ./ackermann 2 5

The range values are as follows:

[0, 2147483646] -not a segmentation fault, but integer overflow
[1, 261876]*    -any higher n-value causes a segmentation fault
[2, 130988]*    -any higher n-value causes a segmentation fault
[4, 14]         -any higher n-value causes a segmentation fault
[4, 1]          -any higher n-value causes a segmentation fault
[6, 0]          -any higher n-value causes a segmentation fault
*NOTE: the n-value that causes a seg fault changes for these
two on each compile. I have reached 130990 without segmentation
faulting, but currently am segmentation faulting at n=130988

--------------------------------------------------------------------

Question 3: to run the program, simply type ./newton [x] [epsilon]
where x is the number to calculate the root of, and epsilon is the
error range.  For example, ./newton 81 0.001
