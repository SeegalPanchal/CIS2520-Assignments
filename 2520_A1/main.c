/*
Name: Seegal Panchal
ID:   1016249
Date: 09/22/2018

This file contains the function prototypes
and library declarations for any functions
used in other programs during this assignment.

*/

/* include these libraries */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* prototypes */
void permute(char *permutation, char *word, int start, int end);
int Ackerman(int m, int n);
double r_newton(double x, double a, double e, int count);
double i_newton(double x, double a, double e);

/*
  The permute function works by backtracking (or placing).

  It sets a character in the initial spot (for example,
  'c' in spot 1), and then calculates every permutation
  where c is in spot 1.

  Paramters:
    permutation: the new permutation made,
    word: the initial word (carbon)
    index: the index of current char being placed in the permuation
    ense: the length of the string
*/
int count = 0;
void permute(char *permutation, char *word, int index, int end)
{
  int i;
  /* base case
  if the you already placed the last char in the permutation */
  if (index > end)
  {
    count ++;
    /* print the word + the current count
    use this if you want to see the numbering of all 720 permutation */
    /* printf("%s %d\n", permutation, count); */

    /* basic print without the count */
    printf("%s\n", permutation);
    /* exit out of the recurse */
    return;
  }
  /* this loops through every character in word */
  for (i = 0; i <= end; i++)
  {
    /* this make sure that the current index has a unique
    character by checking the current 'set' characters*/
    int repeat = 0;
    int j;
    for (j = index-1; j >= 0; j--)
    {
      /* if the current character has already appeared
      previously in the permutation, then its a repeat*/
      if (word[i] == permutation[j]) repeat = 1;
    }
    /* force the iteration of the char loop (outside loop)
    so that you can look at another character to place in the permutation */
    if (repeat) continue;
    else
    {
      permutation[index] = word[i]; /* place the character*/
      permute(permutation, word, index+1, end); /* permute it */
    }
  }
}

/*
  Parameters: takes user inputted m and n values
  Returns: the number calculated by the function
 */
int Ackermann(int m, int n)
{
  if (m <= 0) return n+1;
  else if (n <= 0) return Ackermann(m-1, 1);
  else return Ackermann(m-1, Ackermann(m, n-1));
}

/* prototypes for Newtons method */
/*  Recursive Newton Calculation function
*   @Param: x = initial value, a = new value,
    e = epsilon range, count = number of iteration
*   @Return: returns the a value (will eventually be an approximate square root)
*/
double r_newton(double x, double a, double e, int count)
{
  /* count how many times we recursed */
  count++;
  /* if the a value is within our range or we recursed too much, return */
  if (fabs(a*a-x) <= e || count > 100000) return a;
  /* else, recalculate a and recurse*/
  else return r_newton(x, (a+x/a)/2, e, count);
}

/*  Recursive Newton Calculation function
*   @Param: x = initial value, a = new value, e = epsilon range
*   @Return: returns the a value (will eventually be an approximate square root)
*/
double i_newton(double x, double a, double e)
{
  /* count number of iterations */
  int count = 0;

  /* while not within our approximation range,
  AND while our count is less than 100000 */
  while (fabs(a*a-x) > e && count < 100000)
  {
    /* increment count and calculate a new a value */
    count++;
    a = (a+x/a)/2;
  }
  return a;
}
