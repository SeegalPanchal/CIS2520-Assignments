/*
Name: Seegal Panchal
ID:   1016249
Date: 09/22/2018

This file runs the newton calculations.

To compile, simply type make in the terminal
Or, to specifically compile just newton.c, type make newton

To execute the compiled file, type ./newton
followed by the x-value and the epsilon value
./newton [x] [epsilon] -without the square brackets

*/

#include "main.c"

int main(int argc, char *argv[])
{
  if (argc == 3)
  {
    /* take the user input, store it as a double */
    double a;
    double x = strtod(argv[1], NULL);
    double e = strtod(argv[2], NULL);

    /* x and epsilon must be positive */
    if (x < 0)
    {
      printf("X must be positive.\n");
      exit(0);
    }
    else if (e < 0)
    {
      printf("E must be positive.\n");
      exit(0);
    }

    /* calculate the initial a */
    a = x / 2;

    /* output format
      also calls the newton funtions */
    printf("Recursive: %.16f\nIterative: %.16f\n",
      r_newton(x, a, e, 0), i_newton(x, a, e));
  }
  /* print the error and tell user how to use the program */
  else fprintf(stderr, "Usage: %s [x] [epsilon]\n", argv[0]);

  /* return 0 for successful execution*/
  return 0;
}
