/*
Name: Seegal Panchal
ID:   1016249
Date: 09/22/2018

This file contains the main functions that
executes the code for permuting carbon
*/

/* include these libraries */

#include "main.c"

int main(int argc, char *argv[])
{
  char word[] = "carbon";
  char permutation[] = "zzzzzz";
  permute(permutation, word, 0, 5);
  return 0;
}
