/*
Name: Seegal Panchal
ID:   1016249
Date: 09/22/2018

This file contains the main functions that
executes the code for the ackermann calculator

The range values are as follows:

[0, 2147483646] -not a segmentation fault, but integer overflow
[1, 261876]*    -any higher n-value causes a segmentation fault
[2, 130988]*    -any higher n-value causes a segmentation fault
[4, 14]         -any higher n-value causes a segmentation fault
[4, 1]          -any higher n-value causes a segmentation fault
[6, 0]          -any higher n-value causes a segmentation fault
*NOTE: the n-value that causes a seg fault changes for these
two on each compile. I have reached 130990 without segmentation
faulting, but currently am segmentation faulting at n=130988

*/

#include "main.c"

int main(int argc, char *argv[])
{
  if (argc == 3)
  {
    /* convert m and n to number */
    int m = atoi(argv[1]);
    int n = atoi(argv[2]);
    if (m < 0) /* make sure n is positive */
    {
      printf("Please retry with an M >= 0\n");
      return 1;
    }
    else if (n < 0) /* exit is n is negative */
    {
      printf("Please retry with an N >= 0\n");
      return 1;
    }
    else
    { /* else, run the function with the m and n values */
      printf("%d\n", Ackermann(m, n));
    }
  }
  else
  { /* print this error if command not used properly */
    fprintf(stderr, "Usage: %s [m] [n]\n", argv[0]);
  }

  return 0;
}
