Name: Seegal Panchal
ID:	1016249

Q1:

Assumptions: 	1. A correct expression with the correct number of brackets has been given.
				2. The terminal window is wide enough to accomodate spacing for the vertical tree drawn by part 1
				3. A variable can only be a length of 1 up to 5 characters in length.
				4. Only 4 operators are used --> +, -, *, / 
				
		Otherwise, it's fairly simple. You can make your life easier by running the program by doing ./q1 "expression here"
		as the quotation marks escape the whole line.
		
Q2:

Assumptions:	1. The given file is named f.dat
				2. The file is a 20x10 array of integers that can be parsed without error
				3. The file represents an already created array which we must apply downheap to, as shown in the slidehow, 
					by doing downheap on the last parental node all the way back to the root, instead of downheaping when adding an element.
					
		Just input your own file and it should work fine.
