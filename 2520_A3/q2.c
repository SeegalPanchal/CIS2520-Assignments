/*
Name: Seegal Panchal
ID:   1016249
Date: 11/05/2018

This file generates a heap from given data and then
generates a min-heap using parental downheap algorithm

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>

#define OBJECTS 20
#define SIZE 10

// this structure is just to qualify the data
// convert the 2d array into a 1d array
typedef struct HEAP_NODE
{
    int data[10];
    int key;
} Node;

/*
  Returns the left child index of given parent index
  Paramters:
    The parent index of the node
  Returns:
    the index of the left child, or -1 if it doesnt exist
*/
int left_child_index(int parent_index)
{
    // if it has a left child
    if (2 * parent_index < OBJECTS+1)
    {
        return 2 * parent_index;
    }

    // if there is no left child, return -1
    return -1;
}

/*
  Returns the right child index of given parent index
  Paramters:
    The parent index of the node
  Returns:
    the index of the right child, or -1 if it doesnt exist
*/
int right_child_index(int parent_index)
{
    // if it has a left child
    if (2 * parent_index + 1 < OBJECTS+1)
    {
        return 2 * parent_index + 1;
    }

    // if there is no right child, return -1
    return -1;
}

/*
  Returns the index of the last internal node
  Returns:
    The index of the last internal node
*/
int last_parent_index()
{
    // +1 because we don't use cell 0 so there is n+1 cells
    return (OBJECTS+1) / 2;
}


/*
  Creates a heap node and stores it into an array
  Paramters:
    The array to store the node in
  Returns:
    The created node
*/
Node *create_node(int obj[SIZE])
{
    // create a new node
    Node *node = malloc(sizeof(Node));
    // set each value in data (copy it)
    for (int i = 0; i < SIZE; i ++)
    {
        // set the data
        node->data[i] = obj[i];
    }
    // set the key to the sum of the first 3 integers
    node->key = node->data[0] + node->data[1] + node->data[2];
    // return the node
    return node;
}

/*
  Creates a heap given a 2d array of size 20x10 where the first 3 digits are the key
  Paramters:
    The array to store the node in
*/
void create_heap(int data[OBJECTS][SIZE], Node heap[OBJECTS+1])
{
    for (int x = 1; x < OBJECTS+1; x ++)
    {
        // have to start at 1 because heap doesn't use index 0
        heap[x] = *create_node(data[x-1]);
    }
}

/*
  Swaps two objects in the heap array
  Paramters:
    The array to swap the nodes in
    The index of the first element to swap
    The index of the second element to swap
*/
void swap(Node heap[OBJECTS+1], int a, int b)
{
    // swap the elements
    Node temp = heap[a];
    heap[a] = heap[b];
    heap[b] = temp;
}

/*
  Applies a parental node downheap algorithm to generate a minheap
  Paramters:
    The heap array to downheap
*/
void downheap(Node heap[OBJECTS+1])
{
    // loop through the heap, starting from the last parent
    // and working back to the root
    for (int i = last_parent_index(); i > 0; i --)
    {
        int index = i;
        // while there is a left child
        while (left_child_index(index) > 0)
        {
            // assume the left child is smaller than the right child
            int smaller_child_index = left_child_index(index);
            // change if the right childs key is actually smaller
            if (right_child_index(index) > 0 && heap[right_child_index(index)].key < heap[left_child_index(index)].key)
            {
                smaller_child_index = right_child_index(index);
            }
            // if the current key is smaller than the smaller child
            if (heap[index].key < heap[smaller_child_index].key)
            {
                // exit out of the loop
                break;
            }
            // else, swap the smaller child and the current node
            swap(heap, index, smaller_child_index);
            // continue downheaping the node we just swapped down
            index = smaller_child_index;
        }
    }
}

/*
  Prints the heap array
  Paramters:
    The array of heap nodes to print
*/
void print_heap(Node heap[OBJECTS+1])
{
    // print the heap
    for (int x = 1; x < OBJECTS+1; x++)
    {
        for (int y = 0; y < SIZE; y++)
        {
            printf("%02d ", heap[x].data[y]);
        }
        printf("\n");
    }
}

/*
  Reads a file containing 20x10 integers separated by a space
  Paramters:
    The 2d array to store the data in
*/
void read_file(int data[OBJECTS][SIZE])
{
    FILE *fp = fopen("f.dat", "r");
    // buffer to read line
    char buffer[100];
    // tracker to see what line we are on
    int line_read = 0;
    // loop through the file reading each line
    while(fgets(buffer, 100, fp))
    {
        // tokenize the string on spaces
        int value = atoi(strtok(buffer, " "));
        // for loop through the line read, should be 10 integers
        for (int i = 0; i < SIZE; i ++)
        {
            // store it into the array
            data[line_read][i] = value;
            // continue tokenizing the string
            // but only if theres more values to be read
            if (i < 9) value = atoi(strtok(NULL, " "));
        }
        // increment the line read (now reading the next heap object)
        line_read++;
    }
    fclose(fp);
}

int main()
{
    // the file 20 rows x 10 columns
    int data[OBJECTS][SIZE];
    // heap
    Node heap[OBJECTS+1];
    // pass the heap
    read_file(data);
    // create the heap
    create_heap(data, heap);
    // downheap the heap
    downheap(heap);
    // print the keys

    for (int x = 1; x < OBJECTS+1; x++)
    {
        printf("Key: %d\n", heap[x].key);
    }
    print_heap(heap);
    return 0;
}
