/*
Name: Seegal Panchal
ID:   1016249
Date: 11/05/2018

This file generates a binary tree from an inputted infix expression.
The tree can be outputted using preorder/inorder/postorder traversal.

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>

typedef enum boolean
{
    false = 0,
    true = 1
} Bool;

typedef struct Nodes
{
    // type string so it can store an integer and a character
    char *data;
    // is this a variable?
    Bool variable;
    // does this node have children?
    Bool children;
    // the height value (depth of tree)
    int height;
    // what is the data of the data?
    double value;
    // binary tree
    struct Nodes *left;
    struct Nodes *right;

} Node;

// creates a node and adds it to the tree
Node *create_node(char *data)
{
    if (data == NULL) return NULL;
    // make a node and allocate memory for it
    Node *temp = malloc(sizeof(Node));
    // copy the data over
    temp->data = malloc(sizeof(data));
    strcpy(temp->data, data);
    temp->data[strlen(temp->data)] = '\0';
    // if the data is a number, parse it, otherwise don't
    // error checking for double
    char *endptr;
    errno = 0;
    temp->value = strtod(temp->data, &endptr);
    // it couldn't be parsed into a double
    if (errno != 0 || *endptr != '\0')
    {
        if (temp->data[0] != '*' && temp->data[0] != '/'
        && temp->data[0] != '+' && temp->data[0] != '-')
        // its a variable, reset value to 0
        temp->variable = true;
        temp->value = 0;
    }
    // its not a variable
    else
    {
        temp->variable = false;
    }
    // initialize the other variables
    temp->left = NULL;
    temp->right = NULL;
    temp->height = 0;
    temp->children = false;
    return temp;
}

typedef struct LinkedList
{
    struct LinkedList *next;
    Node *node;
} List;

// global head pointer
// initialized to NULL
List *head = NULL;

// creates a list node
// this is stuff from A2 (this is used for display)
List *create_list(Node *node)
{
    List *temp = malloc(sizeof(List));
    temp->node = malloc(sizeof(node));
    temp->node = create_node(node->data);
    temp->node->height = node->height;
    temp->node->children = node->children;
    temp->next = NULL;
    return temp;
}

// pushes a node to the linked list
void push(Node *node)
{
    // if no node was passed
    if (node == NULL) return;

    List *item = create_list(node);

    // if this is the first item to be pushed to the list
    if (head == NULL) {
        head = item;
        return;
    }

    // add the new node to the list
    List *temp = head;
    // increment to the end of the list
    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    // assuming temp is last item in list, and temp->next == NULL
    temp->next = item;
}

// pops a node from the linked list
Node *pop()
{
    // if there is no list, return null
    if (head == NULL) return NULL;
    // return the data, set head to next
    Node *temp = head->node;
    // increment the head
    head = head->next;
    // return the node
    return temp;
}

// substring helper function
// works exactly like in java, inclusive exclusive
char *substring(int start, int end, char input[])
{
    // if no string
    if (strlen(input) <= 0) return NULL;
    // length of substring
    int length = end-start;
    // create a substring variable
    char *temp = malloc(length);
    // copy the desired string over
    memcpy(temp, &input[start], length);
    // return the substring
    return temp;
}

/*
  Returns the height of the current node
  Paramters:
    The root node of the tree
  Return:
    The height of the current node
*/
int get_height(Node *root)
{
    if (root == NULL) return 0;
    else
    {
       /* compute the depth of each subtree */
       int left_depth = get_height(root->left);
       int right_depth = get_height(root->right);

       /* use the larger one */
       if (left_depth > right_depth)
           return(left_depth+1);
       else return(right_depth+1);
    }
}

/*
  Count the number of nodes in the tree
  Paramters:
    The root node of the tree
*/
int count_nodes(Node *root)
{
    //Node itself should be counted
    int c =  1;

    // if the node deosn't exist, don't count it
    if (root == NULL) return 0;
    else
    {   // increase count by nodes
        c += count_nodes(root->left);
        c += count_nodes(root->right);
        return c;
    }
}

/*
  Prints the tree in preorder notation
  Paramters:
    The root node of the tree
*/
void preorder(Node *root)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // print the data
    printf("%s ", root->data);
    // traverse the left
    preorder(root->left);
    // traverse the right
    preorder(root->right);
}

/*
  Prints the tree in inorder notation
  Paramters:
    The root node of the tree
*/
void inorder(Node *root)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // traverse the left
    if (root->left != NULL) printf("(");
    inorder(root->left);
    // print the data
    printf("%s", root->data);
    // traverse the right
    inorder(root->right);
    if (root->right != NULL) printf(")");
}

/*
  Prints the tree in postorder notation
  Paramters:
    The root node of the tree
*/
void postorder(Node *root)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // traverse the left
    postorder(root->left);
    // traverse the right
    postorder(root->right);
    // print the data
    printf("%s ", root->data);
}

/*
  Sets the height of the tree
  Paramters:
    The root node of the tree
*/
void set_height(Node *root, int height)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // set the height
    root->height = height;
    // traverse the left
    set_height(root->left, height+1);
    // traverse the right
    set_height(root->right, height+1);
}

/*
  Sets if the current node has children
  Paramters:
    The root node of the tree
*/
void set_children(Node *root)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // set the height
    if (root->left != NULL)
    {
        root->children = true;
    }
    // traverse the left
    set_children(root->left);
    // traverse the right
    set_children(root->right);
}

/*
  Creates a linked list in order fashion
  Paramters:
    The root node of the tree
*/
void inorder_list(Node *root)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // traverse the left
    inorder_list(root->left);
    // add the node to the linked list
    push(root);
    // traverse the right
    inorder_list(root->right);
}

/*
  Find the variables inputted by the user
  Paramters:
    The root node of the tree
    The variable inputted by the user
  Returns:
    If the variable was updated successfully
*/
Bool find_var(Node *root, char *var)
{
    if (root == NULL) return false;
    if (strcmp(root->data, var)==0) return true;
    return find_var(root->left, var) || find_var(root->right, var);
}

// loops through the binary tree
// if it finds the variable, updates the variables value
// if it can't update, returns false
Bool update(Node *root, char *var, double value)
{
    if (root == NULL) return false;
    if (strcmp(root->data, var)==0)
        {
        // set the variable, return true
        root->value = value;
        return true;
        }
    return update(root->left, var, value) || update(root->right, var, value);
}

/*
  The code to display the tree, does the following:
    1. Creates a 2d array from a linked list of the binary tree in inorder fashion
    2. stores it left to right in the 2d array based on its height
    3. each value gets it own column in the 2d array\
    4. every other box is a tab
    5. prints the 2d array, then checks where branches should be printed and modifies that
  Paramters:
    The root node of the tree
*/
void display(Node *root)
{
    int x = count_nodes(root);
    int y = get_height(root) * 3 - 2;
    Node *arr[x][y];

    // both height and children could be hid into the other traverses, but I only added it in later so I'm leaving this code here
    // check the height of each node
    set_height(root, 0);
    // check if the node has any children
    set_children(root);
    head = NULL; // create a new linked list
    inorder_list(root);

    // loads the arraylist (and the bars)
    for (int i = 0; i < x; i ++) // columns (width)
    {
        Node *temp = pop();
        for (int j = 0; j < y; j++) // rows (height)
        {
            if (j == (temp->height * 3))
            {
                char *new = malloc(sizeof(temp->data + sizeof("\t")));
                new = temp->data;
                strcat(new, "\t");
                arr[i][j] = create_node(new);
                arr[i][j]->children = temp->children;
                // if you can go above the current operator
                if (j-1 > 0)
                {
                    arr[i][j-1] = create_node("|\t");
                }
            }
            else
            {
                arr[i][j] = create_node("\t");

            }
        }
    }


    // loop through checking every box in the array
    // if the box is an internal node, check if it has children
    // if the node has children, print the branches
    for (int i = 0; i < x; i ++) // columns (width)
    {
        for (int j = 0; j < y; j++) // rows (height)
        {
            // if the current node has children (guarantees that its an operator)
            if (arr[i][j]->children == true)
            {
                arr[i][j+1]->data = "|________";
                // add dashes until the left child is found
                int bar_found = 0;
                for (int k = i-1; k >= 0; k --)
                {
                    // if the left bar hasn't been found
                    if (!bar_found)
                    {
                        arr[k][j+1]->data = "________";
                    }
                    // if you reached the left bar
                    if (strcmp(arr[k][j+2]->data, "|\t")==0
                    || strcmp(arr[k][j+2]->data, "|________")==0
                    || strcmp(arr[k][j+2]->data, "________")==0)
                    {
                        bar_found = 1;
                    }
                }

                // add dashes until the right child is found
                bar_found = 0;
                for (int k = i+1; k < x; k ++)
                {
                    // if you reached the right bar
                    if (strcmp(arr[k][j+2]->data, "|\t")==0
                    || strcmp(arr[k][j+2]->data, "|________")==0
                    || strcmp(arr[k][j+2]->data, "________")==0)
                    {
                        bar_found = 1;
                    }
                    // if the left bar hasn't been found
                    if (!bar_found)
                    {
                        arr[k][j+1]->data = "________";
                    }
                }
                bar_found = 0;
            }
        }
    }
    // print the array
    for (int i = 0; i < y; i ++)
    {
        for (int j = 0; j < x; j++)
        {
            printf("%s", arr[j][i]->data);
        }
        printf("\n");
    }
}

/*
  Prints all the variables defined in the binary tree
  Paramters:
    The root node of the tree
*/
void print_variable(Node *root)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    print_variable(root->left);
    if (root->variable == true) printf("Variable: %s\tValue: %.2lf\n", root->data, root->value);
    // traverse the right
    print_variable(root->right);
}

/*
  Recurses through the binary tree
  Paramters:
    The root node of the tree
    Boolean to determine if a division by zero occured
  Returns:
    the calculated result
*/
double inorder_calculate(Node *root, Bool *division_by_zero)
{
    // should only happen if no tree was passed
    if (root == NULL) return 0;
    // check which operator we are looking at
    if (root->data[0] == '*')
    {
        return inorder_calculate(root->left, division_by_zero) * inorder_calculate(root->right, division_by_zero);
    }
    else if (root->data[0] == '/')
    {
        if (inorder_calculate(root->right, division_by_zero) == 0)
        {
            *division_by_zero = true;
            return 0;
        }
        return inorder_calculate(root->left, division_by_zero) / inorder_calculate(root->right, division_by_zero);
    }
    else if (root->data[0] == '+')
    {
        return inorder_calculate(root->left, division_by_zero) + inorder_calculate(root->right, division_by_zero);
    }
    else if (root->data[0] == '-')
    {
        return inorder_calculate(root->left, division_by_zero) - inorder_calculate(root->right, division_by_zero);
    }
    return root->value;
}

/*
  1. The function strips the outer brackets
  2. Increment until number of opening brackets = number of closing brackets
  3. It's guaranteed that the next read data is an operator
  4. Create an operator internal node
  5. Recursively call this with node_left = left expression (before operator)
  6. node_right = right expression (everything after the operator)

  Basically whats in the assignment guide, drop the outer brackets,
  create a root node for the operator, then recursively read the left
  and right operands.

  Parameters:
    Infix expression, for example: (((x1 + 5.12) * (x2 - 7.68)) / x3)

  Returns:
    Node pointer: the pointer to the node created by this function
*/
Node *calculate_tree(char infix_expression[])
{

    Node *root = NULL;
    int index = 0;

    // length of string
    int length = strlen(infix_expression);
    // if no expression was given
    if (length <= 0) return NULL;
    // if the expression is just a number
    // --> there would be a left parantheses if it was an expression
    if (infix_expression[0] != '(')
    {
        // create the leaf with null branches
        root = create_node(infix_expression);
        // return the leaf
        return root;
    }

    // discard the first and last
    // look for an operator that is not encased in parentheses
    int num_left_par = 0;
    int num_right_par = 0;

    // calculate the index of the operator by:
    //      1. count the number of left brackets (if there is one)
    //      2. find the matching right brackets
    //      3. thats the left subtree, the next index is the operator
    //      4. the rest is the right subtree
    do
    {
        // increment the index
        index++;
        // check if there is another exression (expanding left subtree)
        if (infix_expression[index] == '(')
        {
            num_left_par++;
        }
        else if (infix_expression[index] == ')')
        {
            num_right_par++;
        }
    } while (num_left_par != num_right_par);

    // index should be on an operator or before one
    // if its not on one, increment till it is
    while(infix_expression[index] != '*'
        && infix_expression[index] != '/'
        && infix_expression[index] != '+'
        && infix_expression[index] != '-')
    {
        index++;
    }

    // index is equal to index of the operator
    // create an internal node of that operator
    root = create_node(substring(index, index+1, infix_expression));
    // the left infix expression
    char *left = substring(1, index, infix_expression);
    // the right infix expression
    char *right = substring(index+1, length-1, infix_expression);
    // make left subtree
    root->left = calculate_tree(left);
    // make right subtree
    root->right = calculate_tree(right);
    // return the root
    return root;
}

/*
  This functions flushes the stdin stream and adds a \0
  to the string passed in

  Parameters: (all the lists)
    var: the string to add a \0 to
*/
void flush(char var[])
{
  /* flush any overflowing input from fgets */
  /* if new line dooesnt exist from previous fgets*/
  if (!strchr(var, '\n')) {
    while (fgetc(stdin)!='\n'); /* discard until new line */
    var[strlen(var)] = '\0';
  }
  else var[strlen(var)-1] = '\0';
}

/*
  Gets user
  Paramters:
    The array to store the node in
  Returns:
    The created node
*/
double get_value()
{
    char value_input[12];
    double value;

    printf("Enter a value (without spaces or characters): ");
    // read what the user inputted
    fgets(value_input, 12, stdin);
    // remove extra characters from stdin and add \0
    flush(value_input);

    // check if the value is parseable
    // if the data is a number, parse it, otherwise don't
    // error checking for double
    char *endptr;
    errno = 0;
    value = strtod(value_input, &endptr);
    if (errno != 0 || *endptr != '\0') value = 0;
    while (strlen(value_input) <= 0 || strlen(value_input) > 9
        || errno != 0 || *endptr != '\0')
    {
        printf("Input is not a number, or too small/big. Enter a valid value: ");
        // read what the user inputted
        fgets(value_input, 12, stdin);
        // remove extra characters from stdin and add \0
        flush(value_input);

        // check if the value is parseable
        // if the data is a number, parse it, otherwise don't
        // error checking for double
        errno = 0;
        value = strtod(value_input, &endptr);
        // it couldn't be parsed into a double
        // force iteration of loop
        if (errno != 0 || *endptr != '\0')
        {
            value = 0;
            continue;
        }
    }
    return value;
}

/*
  Loads and prints the menu
  Paramters:
    The user inputted inorder expression
*/
void menu(char input[])
{
    // first, calculate the tree from the given input
    Node *root = calculate_tree(input);

    // input variables
    char user_input[3] = "";
    char var_input[7] = "";
    Bool division_by_zero = false;
    double result = 0;

    while (1) { // infinite loop the menu
        printf("Enter a transaction code:\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s",
        "\t1. Display",
        "\t2. Preorder",
        "\t3. Inorder",
        "\t4. Postorder",
        "\t5. Update",
        "\t6. Calculate",
        "\t7. Exit",
        "Transaction code: ");

        // get the transaction code input
        fgets(user_input, 3, stdin);
        flush(user_input);

        while (strlen(user_input) > 1 || user_input[0] < '1' || user_input[0] > '7')
            {
            printf("Error! Invalid transaction code. Enter a valid transaction code:\n%s",
            "Transaction code: ");
            fgets(user_input, 3, stdin);
            flush(user_input);
            }
        switch(user_input[0])
        {
            case '1': // Display
                display(root);
                print_variable(root);
                break;
            case '2': // Preorder
                printf("Prefix expression: ");
                preorder(root);
                printf("\n");
                break;
            case '3': // Inorder
                printf("Infix expression: ");
                inorder(root);
                printf("\n");
                break;
            case '4': // Postorder
                printf("Postfix: ");
                postorder(root);
                printf("\n");
                break;
            case '5': // Update
                // read variable and integer input
                // pass it to update (the test worked)

                // get the variable input
                printf("Enter the variable name: ");
                fgets(var_input, 7, stdin);
                flush(var_input);

                while (strlen(var_input) <= 0 || strlen(var_input) > 5)
                {
                    printf("Variable length is forbidden, please enter a smaller variable name: ");
                    fgets(var_input, 7, stdin);
                    flush(var_input);
                }

                if (find_var(root, var_input) == false)
                {
                    printf("Variable '%s' not found.\n", var_input);
                    break;
                }
                if (update(root, var_input, get_value()) == true)
                {
                    printf("Variable updated.\n");
                }
                else {
                    printf("Unexpected error, variable no updated.\n");
                }
                break;
            case '6':
                division_by_zero = false;
                result = inorder_calculate(root, &division_by_zero);
                if (division_by_zero == true)
                {
                    printf("Division by 0 error.\n");
                    break;
                }
                printf("The expression evalutes to: %.2lf\n", result);
                break;
            case '7': // Exit
                printf("Exiting the program.\n");
                exit(-1);
            default:
                printf("Error. Input is not 1-7, but passed the initial error check. Exiting program.\n");
                exit(-1);
        }
    }
}

int main(int argc, char* argv[])
{
    // make sure the user entered a postfix expression
    if (argc == 2)
    {
        // contains the infix input
        char *input = argv[1];
        menu(input);
    }
    // else the user hasn't inputted a proper argument
    else
    {
        /* print this error if command not used properly */
        fprintf(stderr, "Usage: %s [Infix Expression]\n", argv[0]);
    }
    // exit
    return 0;
}
