// Seegal Panchal
// November 19th 2018
// A4 CIS*2520

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct Nodes
{
    // the key (i.e. flr1854)
    char *key;
    // number of times the key has appeared
    int count;
    // height of the current node
    int height;
    // binary tree
    struct Nodes *left;
    struct Nodes *right;

} Node;

Node *create_node(char *key)
{
    // create a new node
    Node *new = malloc(sizeof(Node));
    // set the key
    new->key = malloc(sizeof(key));
    strcpy(new->key, key);
    // initialize the other variables
    new->count = 1;
    new->height = 1;
    // children don't exist yet
    new->left = NULL;
    new->right = NULL;
    // return the node
    return new;
}

void free_node(Node *to_free)
{
    // free the mallocd values
    free(to_free->key);
    free(to_free);
}

// Compute the height of a tree
int height(Node* root)
{
   if (root == NULL) return 0;
   return root->height;
}

// return the bigger number of a and b
int max(int a, int b)
{
    return (a > b) ? a : b;
}

Node *rotate_right(Node *root)
{
    // the left subtree
    Node *left_child = root->left;
    // the right subtree of the left child
    Node *sub_right = left_child->right;

    // perform rotation
    left_child->right = root;
    root->left = sub_right;

    // recalculate height
    root->height = 1 + max(height(root->left), height(root->right));
    left_child->height = 1 + max(height(left_child->left), height(left_child->right));

    // return the new root node
    return left_child;
}

Node *rotate_left(Node *root)
{
    // the left subtree
    Node *right_child = root->right;
    // the left subtree of the right child
    Node *sub_left = right_child->left;

    // perform rotation
    right_child->left = root;
    root->right = sub_left;

    // new height
    root->height = 1 + max(height(root->left), height(root->right));
    right_child->height = 1 + max(height(right_child->left), height(right_child->right));

    // return the new root node
    return right_child;
}

Node *balance(Node *root, char *key)
{
    // now the balancing code
    int balance_factor = height(root->left) - height(root->right);

    // left left case
    if (balance_factor > 1 && strcmp(key, root->left->key) < 0)
    {
        return rotate_right(root);
    }

    // right right case
    if (balance_factor < -1 && strcmp(key, root->right->key) > 0)
    {
        return rotate_left(root);
    }

    // left right case
    if (balance_factor > 1 && strcmp(key, root->left->key) > 0)
    {
        root->left = rotate_left(root->left);
        return rotate_right(root);
    }

    // right left case
    if (balance_factor < -1 && strcmp(key, root->right->key) < 0)
    {
        root->right = rotate_right(root->right);
        return rotate_left(root);
    }

    // else return the unchanged root
    return root;
}

// insert a node into the AVL tree
Node *insert(Node *root, char *key)
{
    // check if root is null, then initialize tree
    // or add to the tree as a leaf
    if (root == NULL) return create_node(key);

    // if the key is < current key
    if (strcmp(key, root->key) < 0)
    {
        // recursively insert the key
        root->left = insert(root->left, key);
    }
    // if the key is < current key
    else if (strcmp(key, root->key) > 0)
    {
        // recursively insert the key
        root->right = insert(root->right, key);
    }
    else // check if the key == current node
    {
        // increment the count
        root->count ++;
        return root; // don't need to rebalance because no new node is added
    }

    root->height = 1 + (max(height(root->left), height(root->right)));

    // balance the key we inserted
    return balance(root, key);
}

Node *min_node(Node* root)
{
    Node* temp = root;

    /* loop down to find the leftmost leaf */
    while (temp->left != NULL)
    {
        temp = temp->left;
    }

    return temp;
}

Node *delete_node(Node *root, char *key) {
    // check if root is null, then initialize tree
    // or add to the tree as a leaf
    if (root == NULL) return root;

    // if the key is < current key
    if (strcmp(key, root->key) < 0)
    {
        // recursively insert the key
        root->left = delete_node(root->left, key);
    }
    // if the key is < current key
    else if (strcmp(key, root->key) > 0)
    {
        // recursively insert the key
        root->right = delete_node(root->right, key);
    }
    else // check if the key == current node
    {
        // node with only one child or no child
        if( (root->left == NULL) || (root->right == NULL) )
        {
            Node *temp = (root->left != NULL) ? root->left : root->right;

            // No child case
            if (temp == NULL)
            {
                temp = root;
                root = NULL;
            }
            else // One child case
            {
                //copy the structs
                root->key = realloc(root->key, sizeof(temp->key));
                strcpy(root->key, temp->key);
                root->count = temp->count;
                root->height = temp->height;
                root->left = temp->left;
                root->right = temp->right;
                
            }
            free_node(temp);
        }
        else
        {
            // node with two children: Get the inorder
            // successor (smallest in the right subtree)
            Node* temp = min_node(root->right);

            // Copy the inorder successor's data to this node
            root->key = realloc(root->key, sizeof(temp->key));
            strcpy(root->key, temp->key);
            root->count = temp->count;

            // Delete the inorder successor
            root->right = delete_node(root->right, temp->key);
        }
        
    }

    if (root == NULL) return root;

    // update the height
    root->height = 1 + (max(height(root->left), height(root->right)));

    return balance(root, key);
}

Node *remove_node(Node *root, char *key)
{
    // check if root is null, then initialize tree
    // or add to the tree as a leaf
    if (root == NULL) return root;

    // if the key is < current key
    if (strcmp(key, root->key) < 0)
    {
        // recursively insert the key
        root->left = remove_node(root->left, key);
    }
    // if the key is < current key
    else if (strcmp(key, root->key) > 0)
    {
        // recursively insert the key
        root->right = remove_node(root->right, key);
    }
    else // check if the key == current node
    {
        // decrement the count
        root->count --;
        printf("key: %s, frequency: %d\n", root->key, root->count);
        // if the node needs to be replaced
        if (root->count < 1)
        {
            // node with only one child or no child
            if( (root->left == NULL) || (root->right == NULL) )
            {
                Node *temp = (root->left != NULL) ? root->left : root->right;

                // No child case
                if (temp == NULL)
                {
                    temp = root;
                    root = NULL;
                }
                else // One child case
                {
                    //copy the structs
                    root->key = realloc(root->key, sizeof(temp->key));
                    strcpy(root->key, temp->key);
                    root->count = temp->count;
                    root->height = temp->height;
                    root->left = temp->left;
                    root->right = temp->right;
                    
                }
                free_node(temp);
            }
            else
            {
                // node with two children: Get the inorder
                // successor (smallest in the right subtree)
                Node* temp = min_node(root->right);

                // Copy the inorder successor's data to this node
                root->key = realloc(root->key, sizeof(temp->key));
                strcpy(root->key, temp->key);
                root->count = temp->count;

                // Delete the inorder successor
                root->right = delete_node(root->right, temp->key);
            }
        }
    }

    if (root == NULL) return root;

    // update the height
    root->height = 1 + (max(height(root->left), height(root->right)));

    return balance(root, key);
}

// int count = 0;
void inorder(Node *root, int frequency)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // traverse the left
    inorder(root->left, frequency);
    // print the data
    if (root->count >= frequency)
    {
        printf("key: %s, frequency: %d\n", root->key, root->count);
    }
    // count += root->count;
    // traverse the right
    inorder(root->right, frequency);
}

void count(Node *root, int *size, int *counter)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // traverse the left
    count(root->left, size, counter);
    // print the data
    *size += 1;
    *counter += root->count;
    // traverse the right
    count(root->right, size, counter);
}

void search_tree(Node *root, char *key)
{
    // check if null (no_such_key)
    if (root == NULL)
    {
        printf("no_such_key\n");
        return;
    }

    // check if the key is current node
    if (strcmp(key, root->key) == 0)
    {
        // display the key and frequency
        printf("key: %s, frequency: %d\n", root->key, root->count);
        // exit out of the function
        return;
    }

    // check if key is to the left
    if (strcmp(key, root->key) < 0)
    {
        // display the key and frequency
        search_tree(root->left, key);
        // exit out of the function
        return;
    }

    // check if key is to the right
    if (strcmp(key, root->key) > 0)
    {
        // display the key and frequency
        search_tree(root->right, key);
        // exit out of the function
        return;
    }
}

void check_tree(Node *root, char *key)
{
    // check if null (no_such_key)
    if (root == NULL)
    {
        printf("no_such_key\n");
        return;
    }

    // check if the key is current node
    if (strcmp(key, root->key) == 0)
    {   // exit out of the function
        return;
    }

    // check if key is to the left
    if (strcmp(key, root->key) < 0)
    {
        // display the key and frequency
        check_tree(root->left, key);
        // exit out of the function
        return;
    }

    // check if key is to the right
    if (strcmp(key, root->key) > 0)
    {
        // display the key and frequency
        check_tree(root->right, key);
        // exit out of the function
        return;
    }
}

void free_tree(Node *root)
{
    // check if there is a tree to traverse
    if (root == NULL) return;
    // traverse and free
    free_tree(root->left);
    free_tree(root->right);
    free_node(root);
}

// read file
void read_file(Node **root)
{
    // the files has:
    // a word, separated by spaces
    // there may be an leading space that you remove
    // there are empty lines
    // use !feof
    char input[100];
    FILE *fp = fopen("A4_data_f18.txt", "r");
    // while not end of files (account for spaces)
    while (fscanf(fp, " %s", input))
    {
        if (feof(fp)) break;
        //printf("%s\n", input);
        *root = insert(*root, input);
    }
    fclose(fp);
}

/*
  This functions flushes the stdin stream and adds a \0
  to the string passed in

  Parameters: (all the lists)
    var: the string to add a \0 to
*/
void flush(char var[])
{
  /* flush any overflowing input from fgets */
  /* if new line dooesnt exist from previous fgets*/
  if (!strchr(var, '\n')) {
    while (fgetc(stdin)!='\n'); /* discard until new line */
    var[strlen(var)] = '\0';
  }
  else var[strlen(var)-1] = '\0';
}

char *get_key(char buffer[])
{
  // read what the user inputted
  fgets(buffer, 100, stdin);
  // remove extra characters from stdin and add \0 to the input
  flush(buffer);
  while(strlen(buffer) > 98 || strlen(buffer) < 1)
  {
    printf("Error! Invalid key. Enter a key of size between 1 and 98: ");
    // read what the user inputted
    fgets(buffer, 100, stdin);
    // remove extra characters from stdin and add \0 to the input
    flush(buffer);
  }

  return buffer;
}

void menu()
{
    // input variables
    char user_input[3] = "";
    char key_input[100] = "";
    char *key;
    Node *root = NULL;

    int size = 0;
    int counter = 0;
    int frequency = -1;

    while (1) { // infinite loop the menu
        printf("%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s",
        "1. Initialization",
        "2. Find",
        "3. Insert",
        "4. Remove",
        "5. Check Height and Size",
        "6. Find All (above a given frequency)",
        "7. Exit",
        "Enter a code (1-7) and hit Return",
        "avl/> ");

        // get the transaction code input
        fgets(user_input, 3, stdin);
        flush(user_input);
        switch(user_input[0])
        {
            case '1': // Initialize
                free_tree(root);
                root = NULL;
                read_file(&root);
                // printf("Total: %d\n", count);
                break;
            case '2': // Find
                // user inputs a key
                if (root == NULL) {
                    printf("Tree must be initialized before selecting other options.\n");
                    break;
                }
                printf("find key: ");
                key = get_key(key_input);
                // displays the key and count of the inputted key
                search_tree(root, key);
                break;
            case '3': // Insert
                if (root == NULL) {
                    printf("Tree must be initialized before selecting other options.\n");
                    break;
                }
                printf("insert key: ");
                key = get_key(key_input);
                root = insert(root, key);
                search_tree(root, key);
                break;
            case '4': // Remove
                if (root == NULL) {
                    printf("Tree must be initialized before selecting other options.\n");
                    break;
                }
                printf("remove key: ");
                key = get_key(key_input);
                check_tree(root, key);
                root = remove_node(root, key);
                break;
            case '5': // Check Height, Size, and Total Count
                if (root == NULL) {
                    printf("Tree must be initialized before selecting other options.\n");
                    break;
                }
                size = 0;
                counter = 0;
                count(root, &size, &counter);
                printf("height: %d, size: %d, total count: %d\n", height(root), size, counter);
                break;
            case '6': // Find all (above a given frequency)
                if (root == NULL) {
                    printf("Tree must be initialized before selecting other options.\n");
                    break;
                }
                printf("frequency: ");
                scanf("%d", &frequency);
                while (fgetc(stdin)!='\n'); /* discard until new line */
                if (frequency > -1) inorder(root, frequency);
                break;
            case '7': // Exit
                free_tree(root);
                root = NULL;
                printf("Exiting the program.\n");
                exit(-1);
            default:
                printf("Error! Invalid choice.\n");
                break;
        }
    }
}

int main()
{
    menu();
}
