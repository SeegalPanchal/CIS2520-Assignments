Name:   Seegal Panchal
ID:     1016249
E-mail: panchals@uoguelph.ca

Assumptions:
	- IMPORTANT: THE HEIGHT OF 1 NODE IS 1
	- IMPORTANT: THE FILE IS NAME "A4_data_f18.txt"
    - file is space separated keys
    - I was told to use strcmp by the prof, this means:
        - flr10 < flr9, because strcmp only checks 1 vs 9
    - If you initialize twice, the previous tree is deleted
    - IMPORTANT: a key HAS to be between size 1 and size 98
        - Inputting an erroneous key loops until correct input is entered

- I tried to emulate output to match the handout/guide
- The main menu is continously looped

- Option 6: I don't continously prompt the user if the user inputs a negative frequency
    - I just don't ouput anything on a negative frequency
    - a frequency of "0" ouputs ALL nodes

- The executable was named avltree
    - I assumed the file did not have to be named avltree, only the execution
