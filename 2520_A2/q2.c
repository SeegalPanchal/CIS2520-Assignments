/*
Name: Seegal Panchal
ID:   1016249
Date: 10/15/2018

This file calculates a given expression in its postfix format.
For example, 25+ = 2+5 = 7.

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/*
  Defines what a stack is. Holds an integer and points to the item below it.
*/
struct stack
{
  double n;
  struct stack *next;
};

/*
  This function pushes an integer onto the top of the stack.

  Paramters:
    stack **top: the top of the stack, first item out
*/
void push(struct stack **top, double n)
{
  // create a new stack item with the variables
  struct stack *new = malloc(sizeof(struct stack)); // create a new node
  new->n = n; // the integer being added to the stack
  new->next = *top; // the item below is the previous top item
  *top = new; // the new top item is the item just added
  return;
}

/*
  This function pops out the top stack structure, returns the int and
  frees the structure. The stacks top is now the item below it.

  Paramters:
    stack **top: the first item in the stack

  Returns:
    the integer value the top stack struct held
*/
double pop(struct stack **top)
{
  double n;
  struct stack *temp;
  if (*top == NULL) return 0; // if the stack is empty, return 0
  temp = *top; // temp hold the stack item so we can remove it
  *top = temp->next; // make the top the second stack item (remove first stack item)
  n = temp->n; // hold n so we can remove the stack item but return the int it held
  free(temp); // free the removed first stack item
  return n; // return the integer from the stack struct we deleted
}

/*
  This function frees the entire stack at once

  Paramters:
  stack **top = pointer to the stack
*/
void freeStack(struct stack **top)
{
  while(*top != NULL) // loop through the stack
  {
    struct stack *temp = *top; // temporarily hold the first item
    *top = (*top)->next; // increment the stack
    free(temp); // free the item we were holding
  }
}

/*
  This functiion calculates the post fix expression and returns the value
  calculated

  Paramters:
    char *input: the input the user put in (the postfix expression)

  Returns:
    the calculated output
*/
double calculatePostfix(char *input)
{
  struct stack *top = NULL; // make a stack
  for (int i = 0; i < strlen(input); i++) // loop through the input
  {
    double num1;
    double num2;
    double result;
    switch(input[i]) // is the value a number or an operator?
    {
      case '*': // if the multiply operator

        // pop the first two ints
        num2 = pop(&top);
        num1 = pop(&top);
        // multiple the first two ints
        result = num1*num2;
        //print the equation and result
        printf("%.2f * %.2f = %.2f\n", num1, num2, result);
        // push the result back onto the stack
        push(&top, num1*num2);
        break;
      case '/': // if the division operator
        // pop the first two ints
        num2 = pop(&top);
        num1 = pop(&top);
        // calculate the result of the division
        result = num1/num2;
        // print the equation
        printf("%.2f / %.2f = %.2f\n", num1, num2, result);
        // push the result onto the stack
        push(&top, num1/num2);
        break;
      case '+':
        // pop the first two ints
        num2 = pop(&top);
        num1 = pop(&top);
        // calculate the result
        result = num1+num2;
        // print the equation
        printf("%.2f + %.2f = %.2f\n", num1, num2, result);
        // push the result onto the stack
        push(&top, num1+num2);
        break;
      case '-':
        // pop the first two ints
        num2 = pop(&top);
        num1 = pop(&top);
        // calculate the result of the substraction
        result = num1-num2;
        // print the equation
        printf("%.2f - %.2f = %.2f\n", num1, num2, result);
        // push the equation onto the stack
        push(&top, num1-num2);
        break;
      default:
        // push after making sure it is a number
        if (isdigit(input[i])) push(&top, (input[i])-'0');
        break;
    }
  }

  // if the stack
  // else, return the final result (size is 1)
  return pop(&top);
}


int main(int argc, char* argv[])
{
  if (argc == 2) // make sure the user entered a postfix expression
  {
    char *input = argv[1]; // contains the postfix input
    printf("Input: %s\n", input);
    printf("Result: %.2f\n", calculatePostfix(input));
  }
  else
  { /* print this error if command not used properly */
    fprintf(stderr, "Usage: %s [Postfix Expression]\n", argv[0]);
  }

  return 0;
}
