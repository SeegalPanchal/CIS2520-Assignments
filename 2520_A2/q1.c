/*
Name: Seegal Panchal
ID:   1016249
Date: 10/15/2018

This file is the program for the car rental company.
The program takes one of 7 inputs from the user. The user can
rent a car or return a car. The program intializes by reading
from a data file. The program exits after updating the file and
outputting the amount of money earned by the car rental company
on that day, meaning how much people payed when they returned their
car.

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

// structure that defines what a Car is
typedef struct Cars
{
  char *plateNumber;
  int returnDate;
  int mileage;
} Car;

// structure for a list of cars
typedef struct Lists
{
  Car *car;
  struct Lists *next;
} List;

// CAR SUPPORT FUNCTIONS -------------------------------------------------------

/*
  Car function that creates a new car struct and returns it.

  Parameters:
    plate number: the license plate as a string
    return date: the date of a rented cars expected return
    mileage: the mileage the car has driven in total

  Returns:
    A pointer to a car structure
*/
Car *new_car(char *plateNumber, int returnDate, int mileage)
{
  // malloc memory for a new car
  Car *car = malloc(sizeof(Car));
  // malloc memory for the plate number
  car->plateNumber = malloc(sizeof(plateNumber));
  // copy the plate string over to the new car
  strcpy(car->plateNumber, plateNumber);
  // set the return date
  car->returnDate = returnDate;
  // set the mileage
  car->mileage = mileage;
  // return the car struct
  return car;
}

/*
  This function formats and prints the Car you want to print the values of.

  Parameters:
    car pointer: a pointer to the car you want to print
*/
void print_car(Car *car)
{
  // print all the variables from the car
  printf("License plate: %s\tExpected return date: %d        Mileage: %d\n", car->plateNumber, car->returnDate, car->mileage);
  // end of function
  return;
}

/*
  this function properly frees everything malloced
  inside a car structure, including the structure

  Parameters:
    car pointer: points to the car you want to free
*/
void free_car(Car *car)
{
  // free the plate string
  free(car->plateNumber);
  // free the struct
  free(car);
  // return out
  return;
}


/*
  This functions adds a car to the available list

  Parameters:
    list pointer pointer: points to the var that points to the list
    car pointer: points to the car you want to add to the list
*/
void push_available(List **list_ptr, Car *car)
{
  // two temp points, so we can insert a node into the middle of the list
  List *current = *list_ptr;
  List *previous = NULL;

  // while the milage on the car being added
  // is higher than the current first available car
  // go to the next car
  while (current != NULL && car->mileage > current->car->mileage)
  {
    previous = current;
    current = previous->next;
  }

  // make a new available for rent node
  List *new = malloc(sizeof(List));
  // put the car in the node
  new->car = car;
  // car points to:
  //  1. a car that has less milelage than this new car
  //  2. null, in which case this is the first car
  new->next = current;

  // if there is a car with less mileage than the car being added,
  // the previous car points to this car
  if (previous != NULL) previous->next = new;

  // we are past the above loop, if the current node == the list ptr
  // this means that this car has lower mileage than the head
  // make this new car the head
  if (current == *list_ptr) *list_ptr = new;
}

/*
  this function properly frees everything malloced
  inside a car structure, including the structure

  Parameters:
    list pointer pointer: points to the var that points to the list

  Returns:
    car pointer: returns the car that was removed from the list
*/
Car *pop_available(List **list_ptr)
{
  // if there is no list, then what are we going to pop?
  if (*list_ptr == NULL) return NULL;
  // if there is a list, store the first node temporarily
  List *temp = *list_ptr;
  // set up the list to pop the first node
  // set the first node in the list to be the second element
  *list_ptr = temp->next;
  // store the car in temp so we can remove the node
  // without losing access to the car
  Car *car = temp->car;
  // free the head popped node
  free(temp);
  // return the car to the user
  return car;
}

/*
  This function adds a car to the rented list

  Parameters:
    list pointer pointer: points to the var that points to the list
    Car pointer: points to the car you are adding to the list
*/
void push_rented(List **list_ptr, Car *car)
{
  // two temp points, so we can insert a node into the middle of the list
  List *current = *list_ptr;
  List *previous = NULL;

  // while the milage on the car being added
  // is higher than the current first available car
  // go to the next car
  while (current != NULL && car->returnDate > current->car->returnDate)
  {
    previous = current;
    current = previous->next;
  }

  // make a new available for rent node
  List *new = malloc(sizeof(List));
  // put the car in the node
  new->car = car;
  // car points to:
  //  1. a car that has less milelage than this new car
  //  2. null, in which case this is the first car
  new->next = current;

  // if there is a car with less mileage than the car being added,
  // the previous car points to this car
  if (previous != NULL) previous->next = new;

  // we are past the above loop, if the current node == the list ptr
  // this means that this car has lower mileage than the head
  // make this new car the head
  if (current == *list_ptr) *list_ptr = new;
}

/*
  This function removes a car from the rented list

  Parameters:
    list pointer pointer: points to the var that points to the list
    plate number: the license of the car to be removed from the list

  Returns:
    car pointer: returns the car that was removed from the list
*/
Car *pop_rented(List **list_ptr, char *plateNumber)
{
  // two temp points, so we can insert a node into the middle of the list
  List *current = *list_ptr;
  List *previous = NULL;

  // loop through the list looking for a car with the same plate number
  while (current != NULL && strcmp(plateNumber, current->car->plateNumber) != 0)
  {
    previous = current;
    current = previous->next;
  }

  // if the car with the correct plate is found
  if (current != NULL && strcmp(plateNumber, current->car->plateNumber) == 0)
  {
    // connect the list around our current car (pop out the current node)
    if (previous) previous->next = current->next;
    else *list_ptr = current->next;
    // store the car from the node so we can free the node
    Car *temp = current->car;
    // free up the nodes memory
    free(current);
    // car is returned
    temp->returnDate = 0;
    // return the car we found
    return temp;
  }

  // else we didnt find a car with that plate number
  return NULL;
}

/*
  This function adds a car to the repair list

  Parameters:
    list pointer pointer: points to the var that points to the list
    car pointer: the car to be added to the list
*/
void push_repair(List **list_ptr, Car *car)
{
  // make a new available for rent node
  List *new = malloc(sizeof(List));
  // put the car in the node
  new->car = car;
  // new points to the old head
  new->next = *list_ptr;
  // the new head is the newly added car
  *list_ptr = new;
}

/*
  This function removes a car from the repair list

  Parameters:
    list pointer pointer: points to the var that points to the list
    plate number: the license of the car to be removed from the list

  Returns:
    car pointer: returns the car that was removed from the list
*/
Car *pop_repair(List **list_ptr, char *plateNumber)
{
  // two temp points, so we can insert a node into the middle of the list
  List *current = *list_ptr;
  List *previous = NULL;

  // loop through the list looking for a car with the same plate number
  while (current != NULL && strcmp(plateNumber, current->car->plateNumber) != 0)
  {
    previous = current;
    current = previous->next;
  }

  // if the car with the correct plate is found
  if (current != NULL)
  {
    // connect the list around our current car (pop out the current node)
    if (previous) previous->next = current->next;
    else *list_ptr = current->next;
    // store the car from the node so we can free the node
    Car *temp = current->car;
    // free up the nodes memory
    free(current);
    // return the car we found
    return temp;
  }

  // else we didnt find a car with that plate number
  return NULL;
}

/*
  This functions prints an entire list

  Parameters:
    list pointer pointer: points to the var that points to the list
      that you want to print
*/
void print_list(List **list_ptr)
{
  // store the head pointer in a temp so we don't
  // lose track of the first node in another function
  List *current = *list_ptr;

  //loop through the list
  while (current)
  {
    //print the car
    print_car(current->car);
    // increment the list
    current = current->next;
  }
  //end of function
  return;
}

/*
  This function removes the first car of any list

  Parameters:
    list pointer pointer: points to the var that points to the list
        that you want to remove a car from
*/
Car *pop(List **list_ptr)
{
  // if there is no list, then what are we going to pop?
  if (*list_ptr == NULL) return NULL;
  // if there is a list, store the first node temporarily
  List *temp = *list_ptr;
  // set up the list to pop the first node
  // set the first node in the list to be the second element
  *list_ptr = temp->next;
  // store the car in temp so we can remove the node
  // without losing access to the car
  Car *car = temp->car;
  // free the head popped node
  free(temp);
  // return the car to the user
  return car;
}

/*
  This writes all the lists to a text file

  Parameters: (all three lists)
    available pointer pointer: the available list
    rented pointer pointer: the rented list
    repair pointer pointer: the repair list
*/
void write_file(List **available, List **rented, List **repair)
{
  FILE *fp = fopen("lists.txt", "w");
  Car *car;

  fprintf(fp, "%s\n", "AVAILABLE-FOR-RENT"); // write available for available heading
  while (*available) {
    // pop the car from the list
    car = pop(available);
    // write it to the file
    fprintf(fp, "%s,%d,%d\n", car->plateNumber, car->returnDate, car->mileage);
    // free the car
    free_car(car);
  }

  fprintf(fp, "%s\n", "RENTED"); // write RENTED for rent heading
  while (*rented) {
    // pop the car from the list
    car = pop(rented);
    // write it to the file
    fprintf(fp, "%s,%d,%d\n", car->plateNumber, car->returnDate, car->mileage);
    // free the car
    free_car(car);
  }

  fprintf(fp, "%s\n", "IN-REPAIR"); // write IN-REPAIR for repair heading
  while (*repair) {
    // pop the car from the list
    car = pop(repair);
    // write it to the file
    fprintf(fp, "%s,%d,%d\n", car->plateNumber, car->returnDate, car->mileage);
    // free the car
    free_car(car);
  }

  fclose(fp);
}

/*
  This functions reads the file and initializes the lists

  Parameters: (all the lists)
    available pointer pointer: the available list
    rented pointer pointer: the rented list
    repair pointer pointer: the repair list
*/
void read_file(List **available, List **rented, List **repair)
{
  FILE *fp = fopen("lists.txt", "r");

  Car *car;
  char *plateNumber;
  int returnDate = 0;
  int mileage = 0;

  char buffer[100]; // buffer to read file
  int list = 0;

  while(fgets(buffer, 100, fp)) // loop through the file reading each line
  {
    buffer[strlen(buffer)-1] = '\0';

    if (strcmp(buffer, "AVAILABLE-FOR-RENT") == 0)
    {
      list = 1; // if this is the available for rent lists
      continue; // loop again cause this is useless inputs
    }
    else if (strcmp(buffer, "RENTED") == 0)
    {
      list = 2; // if its the rented list
      continue; // loop again cause this is useless input
    }
    else if (strcmp(buffer, "IN-REPAIR") == 0)
    {
      list = 3; // if its the in-repair list
      continue; // loop again cause this is useless input
    }
    else // else you are reading data
    {
      // tokenize the string at each delimiter (comma)
      plateNumber = strtok(buffer, ",");
      returnDate = atoi(strtok(NULL, ","));
      mileage = atoi(strtok(NULL, ","));
      // make a new car with the variables
      car = new_car(plateNumber, returnDate, mileage);
      // push these cars onto their respective lists
      switch (list)
      {
        case 1:// available list
          push_available(available, car);
          break;
        case 2: // rented list
          push_rented(rented, car);
          break;
        case 3: // in repair list
          push_repair(repair, car);
          break;
        default:
          printf("This happens if there was no cars in one of the lists. Not an error. Continue.\n");
          break;
      }
    }
  }

  fclose(fp);
}

/*
  This functions flushes the stdin stream and adds a \0
  to the string passed in

  Parameters: (all the lists)
    var: the string to add a \0 to
*/
void flush(char var[])
{
  /* flush any overflowing input from fgets */
  /* if new line dooesnt exist from previous fgets*/
  if (!strchr(var, '\n')) {
    while (fgetc(stdin)!='\n'); /* discard until new line */
    var[strlen(var)] = '\0';
  }
  else var[strlen(var)-1] = '\0';
}

/*
  This functions gets the mileage from the user

  Paramters:
    int car_mileage: the milage of the car before so we know if the user
      is inputting a lower mileage which is wrong

  Returns:
    the mileage as an int
*/
int get_mileage(int car_mileage)
{
  char mileageInput[12];
  int mileage = 0;
  int check = -1;

  printf("Enter a mileage (without spaces or characters): ");
  // read what the user inputted
  fgets(mileageInput, 12, stdin);
  // remove extra characters from stdin and add \0
  flush(mileageInput);

  // loop through the string and check if they put in anything other than numbers
  for (int i = 0; i < strlen(mileageInput); i ++)
  {
    // if its not a digit (letter) == 0
    if (isdigit(mileageInput[i]) == 0) {
      check = 0;
      break;
    }
    else check = -1;
  }

  if (check != 0) mileage = atoi(mileageInput);

  while (strlen(mileageInput) < 1 || strlen(mileageInput) > 10 || check == 0 || mileage < 0 || mileage < car_mileage)
  {
    printf("Error, invalid mileage! Enter a mileage (without spaces or characters): ");
    // read what the user inputted
    fgets(mileageInput, 12, stdin);
    // remove extra characters from stdin and add \0
    flush(mileageInput);
    // loop through the string and check if they put in anything other than numbers
    for (int i = 0; i < strlen(mileageInput); i ++)
    {
      // if its not a digit (letter) == 0
      if (isdigit(mileageInput[i]) == 0) {
        check = 0;
        break;
      }
      else check = -1;
    }

    mileage = atoi(mileageInput);
  }

  return mileage;
}

/*
  This functions gets the plate number from the user

  Paramters:
    char buffer[] = the buffer variable that can read the input

  Returns:
    the plate number cleaned up
*/
char *get_plate_number(char buffer[])
{

  printf("Enter a plate number: ");
  // read what the user inputted
  fgets(buffer, 9, stdin);
  // remove extra characters from stdin and add \0 to the input
  flush(buffer);
  while(strlen(buffer) > 7 || strlen(buffer) < 1)
  {
    printf("Error! Invalid plate number. Enter a 7 digit plate number with no spaces: ");
    // read what the user inputted
    fgets(buffer, 9, stdin);
    // remove extra characters from stdin and add \0 to the input
    flush(buffer);
  }

  return buffer;
}

/*
  This functions gets the return date from the user

  Returns:
    a cleaned up integer > 0 but < 999999 from the user
*/
int get_return_date()
{
  // size 6 for yymmdd, 7 with \0, and 8 to check if user inputted too much
  char dateInput[8];
  int date = 0;
  int check = -1;

  printf("Enter a date (yymmdd): ");
  // read what the user inputted
  fgets(dateInput, 8, stdin);
  // remove extra characters from stdin and add \0
  flush(dateInput);

  // loop through the string and check if they put in anything other than numbers
  for (int i = 0; i < strlen(dateInput); i ++)
  {
    // if its not a digit (letter) == 0
    if (isdigit(dateInput[i]) == 0) {
      check = 0;
      break;
    }
    else check = -1; // else there is only numbers
  }

  // convert it to integer
  if (check != 0) date = atoi(dateInput);

  while (strlen(dateInput) < 1 || strlen(dateInput) > 10
  || check == 0 || date < 1 || date > 999999)
  {
    printf("Error, invalid mileage! Enter a date in (yymmdd) format (without spaces or characters): ");
    // read what the user inputted
    fgets(dateInput, 8, stdin);
    // remove extra characters from stdin and add \0
    flush(dateInput);
    for (int i = 0; i < strlen(dateInput); i ++)
    {
      // if its not a digit (letter) == 0
      if (isdigit(dateInput[i]) == 0) {
        check = 0;
        break;
      }
      else check = -1;
    }

    date = atoi(dateInput);
  }

  return date;
}

/*
  This is where the magic happens.
*/
int main()
{

  // initialize the three lists
  List *available = NULL;
  List *rented = NULL;
  List *repair = NULL;

  read_file(&available, &rented, &repair);

  char input[3];
  int mileage = 0;
  int mileageDriven = 0;
  int returnDate = 0;
  char buffer[9];
  char *plateNumber;
  Car *car = NULL;

  double income = 0;

  while (1) { // infinite loop the menu
    printf("Enter a transaction code:\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s",
    "\t1. add a new car to the available-for-rent list.",
    "\t2. add a returned car to the avilable-for-rent list.",
    "\t3. add a returned car to the repair list.",
    "\t4. transfer a car from the repair list to the available-for-rent list.",
    "\t5. rent the first available car.",
    "\t6. print all the lists.",
    "\t7. quit.",
    "Transaction code: ");

    // get the transaction code input
    fgets(input, 3, stdin);
    flush(input);

    while (strlen(input) > 1 || input[0] < '1' || input[0] > '7')
    {
      printf("Error! Invalid transaction code. Enter a valid transaction code:\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s",
      "\t1. add a new car to the available-for-rent list.",
      "\t2. add a returned car to the avilable-for-rent list.",
      "\t3. add a returned car to the repair list.",
      "\t4. transfer a car from the repair list to the available-for-rent list.",
      "\t5. rent the first available car.",
      "\t6. print all the lists.",
      "\t7. quit.",
      "Transaction code: ");
      fgets(input, 3, stdin);
      flush(input);
    }

    switch(input[0])
    {
      case '1': // user wants to add a car to the available for rent list
        // get the plate number
        plateNumber = get_plate_number(buffer);
        // no car to test mileage against
        mileage = get_mileage(-1);

        printf("Car with plate number %s is added to the available list.\n", plateNumber);

        push_available(&available, new_car(plateNumber, returnDate, mileage));
        break;
      case '2':
        plateNumber = get_plate_number(buffer);

        // pop the car out of the rented list
        car = pop_rented(&rented, plateNumber);
        if (car == NULL) {
          printf("Car does not exist.\n");
          break;
        }

        mileage = get_mileage(car->mileage);

        // used to calculate money
        mileageDriven = mileage - car->mileage;

        // calculate the income earned from the cars returned
        // flat rate of 40 up to 100 miles
        if (mileageDriven <= 100) income += 40.00;
        // 15 cents for every mile more
        else income = 40 + (mileageDriven-100)*0.15 + income;

        // sets the new car mileage
        car->mileage = mileage;
        // pushes the car onto the available list
        push_available(&available, car);
        // car moved from rented to available
        printf("Car with plate number %s is moved from the rented list to the available list.", car->plateNumber);
        break;
      case '3':
        plateNumber = get_plate_number(buffer);

        // pop the car out of the rented list
        car = pop_rented(&rented, plateNumber);
        if (!car) {
          printf("Car does not exist!\n");
          break;
        }

        // get the mileage of the car
        mileage = get_mileage(car->mileage);
        // used to calculate money
        mileageDriven = mileage - car->mileage;

        // calculate the income earned from the cars returned
        // flat rate of 40 up to 100 miles
        if (mileageDriven <= 100) income += 40.00;
        // 15 cents for every mile more
        else income = 40 + (mileageDriven-100)*0.15 + income;

        // sets the new car mileage
        car->mileage = mileage;
        // pushes the car onto the available list
        push_repair(&repair, car);
        printf("Car with plate number %s is moved from the rented list to the repair list.", car->plateNumber);
        break;
      case '4':
        plateNumber = get_plate_number(buffer);

        // pop the car out of the rented list
        car = pop_repair(&repair, plateNumber);
        if (!car) {
          printf("Car does not exist!\n");
          break;
        }

        // pushes the car onto the available list
        push_available(&available, car);
        printf("Car with plate number %s is moved from the repair list to the available list.", car->plateNumber);

        break;
      case '5':
        // get the first available car
        car = pop_available(&available);
        if (!car) {
          printf("Car does not exist!\n");
          break;
        }

        //get the return date from user
        returnDate = get_return_date();
        // set the car return date
        car->returnDate = returnDate;

        // pushes the car onto the rented list
        push_rented(&rented, car);
        printf("Car with plate number %s was rented.", car->plateNumber);

        break;
      case '6': // print all the lists
        printf("\nAvailable cars:\n");
        print_list(&available);
        printf("\nRented cars:\n");
        print_list(&rented);
        printf("\nCars in repair:\n");
        print_list(&repair);
        printf("\n");
        break;
      case '7':
        if (income == 0) printf("Total income: $%.2f\n%s\n", income,
          "No cars were returned during this session.");
        else printf("Total income: $%.2f\n", income);
        write_file(&available, &rented, &repair);
        exit(-1);
      default:
        printf("Error. Input is not 1-7, but passed the initial error check. Exiting program.\n");
        exit(-1);
    }
  }
  return 0;
}
