Name: Seegal Panchal
ID:   1016249
Date: 10/15/2018

Q1.c:

- Type ./q1
- This will bring up a menu with 7 options. Just type 1 through 7, its self-explanatory.

NOTE:
  license plate  = can only be 7 or less characters, but > 1 characters
  return date    = must be between -1 < return date < 999999
  mileage        = can be 10 digits from 0 to 9999999999


Sample output:
1. Prints the license plate of the car added
2. Either the car wasn't found, OR, "Car [license plate] was moved from rented to available."
3. Either the car wasn't found, OR, "Car [license plate] was moved from rented to repair."
4. Either the car wasn't found, OR, "Car [license plate] was moved from repair to available."
5. Either the car wasn't found, OR, "Car [license plate] was rented!"
6. Prints all the lists in [License Plate] - [Return Date] - [Mileage] format
7. Prints the income, OR, prints 0 and says no cars were returns during this session.

NOTE FOR Q1: If you test with an empty file, make sure there isn't 2 lines
  because then, the file isn't really empty. Theres an empty line. And this
  causes a segmentation fault. I can fix this, but it shouldn't be a tested input
  because the file should only be created through the use of the program,
  and the program does not fail to delete the file properly.

Q2.c:

-Type ./q2 [Postfix expresssion]
-e.g., ./q2 25+

Sample output:

  Input: 25+35-*
  2 + 5 = 7
  3 - 5 = -2
  7 * -2 = -14
  Result = -14

- Outputs what the user inputted,
  followed by every calculation the program does,
  followed by the final result.
